# 1lieferschein-xsd
## What is this?
This repository holds basic schema definitions for 1Lieferschein profiles.
In theory, they can be used to perform a XSD validation on the basic structure of 1Lieferschein files.
Only requirement to perform this validation is a verification engine with full support for XML 1.0, which should be available on all platforms.

## Limitations
As 1Lieferschein is an extension for the existing UBL specification and does not introduce own XML namespaces, 
these files are somewhat limited in scope, but provide a good base for a schema validation process.
Some validation engines throw warnings for duplicate namespace imports which can be ignored.
This is caused by mismatching file paths for the equal namespaces.

## Example validation using xmllint
An easy validation can be performed using the `xmllint` package on unix systems.
```
xmllint --schema 1LS-Default-DespatchAdvice.xsd  DespatchAdvice.xml --noout --nowarning
```