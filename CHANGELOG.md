### v0.7.0
- Support for UBL 2.4 types.

### v0.6.2
- Relax CustomizationID, for testing phase version string is not required. Values like 1Lieferschein will pass for now.

### v0.6.1
- BuyerCustomerParty, DeliveryCustomerParty, DespatchSupplierParty, OriginatorCustomerParty and SellerSupplierParty
  - **If** Party (0..1) child is used PartyLegalEntity->PartyRegistrationName is required
    - Party(0..1)->PartyLegalEntity(1..1)->PartyRegistrationName (1..1)

### v0.6.0
- Date/time types require Timezone (mandatory): Z, +00:00, +02:00, -02:00
- UBLVersionID (1..1): "2.X", e.g. 2.3
- CustomizationID (1..1): "1Lieferschein:0.X" (Version below 1.0 during testing phase)
- ProfileID (1..1) "default", additional profiles will follow
- UUID (1..1): Format unspecified, but using lower case UUIDv4 is highly recommended
- Signature (1..1)
  - ReasonCode (1..1): Codeliste "SignatureReasonCodes"
  - ValidationDate (1..1): date of xml generation
  - ValidationTime (1..1): time of xml generation
  - ValidatorID (1..1): A string describing the system which created this xml instance e.g. bobbie_1ls_v1.3.1
  - SignatoryParty (1..1)
    - PartyLegalEntity(1..1)->PartyRegistrationName (1..1)